#!/usr/bin/env python

import sys
from setuptools import setup
from setuptools.command.test import test as TestCommand


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to py.test")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        self.pytest_args = []

    def finalize_options(self):
        TestCommand.finalize_options(self)
        self.test_args = []
        self.test_suite = True

    def run_tests(self):
        # import here, cause outside the eggs aren't loaded
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)

setup(name='dune-buildbot',
      version='0.1',
      description='Buildbot tools for systemtesting in DUNE',
      author='Dominic Kempf <dominic.kempf@iwr.uni-heidelberg.de>, Timo Koch <timo.koch@iws.uni-stuttgart.de>',
      author_email='no_mailinglist_yet@dune-testtools.de',
      url='http://conan2.iwr.uni-heidelberg.de/git/quality/dune-buildbot',
      packages=['dune_buildbot.process', 'dune_buildbot.steps'],
      install_requires=['pyparsing', 'buildbot'],
      tests_require=['pytest', 'pytest-pep8'],
      cmdclass={'test': PyTest})
