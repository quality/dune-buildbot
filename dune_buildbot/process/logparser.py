from pyparsing import Literal, nums, Word, alphas, alphanums, printables, Optional, Group, restOfLine, LineEnd, LineStart, White, OneOrMore, replaceWith
import os.path


class CTestLogParser(object):
    """A parser for ctest output"""

    def __init__(self, path=""):
        self._path = path
        self._parser = self.construct_bnf()

        # the log to be accessed from outside
        self.log = {}
        self.test_names = {}
        self.failed_tests = {}
        self.skipped_tests = {}
        self.summary = ""

    def construct_bnf(self):
        """ The EBNF for a ctest log file. """
        # one line of the stdout+stderr output of a single test #.setParseAction(self.add_test_log)
        test_output = (Word(nums) + Literal(":") + Optional(restOfLine)).setParseAction(self.collect_test_output)
        # a test result line
        test_result = (Word(nums + "/") + Word("Test #") + Word(nums) + Literal(":") + Word(alphanums + "_-") +
                       Word(".") + Word(alphas + "*" + ":" + " ") + restOfLine).setParseAction(self.collect_test_result)
        # a line starting a test
        test_start = (Literal("Start") + Word(nums) + Literal(":") + Word(alphanums + "_-")).setParseAction(self.collect_tests)
        # a line with the test number
        test_number = Group(Literal("test") + Word(nums)).suppress()
        # summary
        test_summary_title = (Literal("Test project") + restOfLine).setParseAction(self.collect_test_summary)
        test_summary_passed = (Group(Word(nums) + Literal("%")) + Literal("tests passed") + restOfLine).setParseAction(self.collect_test_summary)
        test_summary_time = (Literal("Total Test time") + restOfLine).setParseAction(self.collect_test_summary)

        # a line is either one of the above or not important
        line = (test_output | test_result | test_start | test_number | test_summary_time | test_summary_passed) + LineEnd()
        return line

    def collect_test_output(self, origString, loc, tokens):
        # store the test output in the log dictionary
        if tokens[0].strip() in self.log:
            self.log[tokens[0].strip()] = self.log[tokens[0].strip()] + origString.lstrip(tokens[0] + tokens[1]) + '\n'
        else:
            self.log[tokens[0].strip()] = origString.lstrip(tokens[0] + tokens[1]) + '\n'

    def collect_test_result(self, origString, loc, tokens):
        try:
            # store the test output in the log dictionary
            if tokens[2].strip() in self.log:
                self.log[tokens[2].strip()] = self.log[tokens[2].strip()] + origString.lstrip(tokens[0]) + '\n'
            else:
                self.log[tokens[2].strip()] = origString.lstrip(tokens[0]) + '\n'

            # add the test result also to the summary
            self.summary = self.summary + origString + '\n'

            # Check the return code for failure
            if "Passed" not in tokens[6]:
                if "Skipped" in tokens[6]:
                    self.skipped_tests.update({tokens[2].strip(): tokens[6].strip().strip('*')})
                else:
                    self.failed_tests.update({tokens[2].strip(): tokens[6].strip().strip('*')})

        except Exception as e:
            print(e)

    def collect_tests(self, origString, loc, tokens):
        self.test_names.update({tokens[1].strip(): tokens[3].strip()})

    def collect_test_summary(self, origString, loc, tokens):
        self.summary = self.summary + origString + '\n'

    def compile(self, line):
        self._parser.parseString(line)


class GCCLogParser(object):
    """A parser for make output with gcc"""

    # TODO provide message highlighting and direct HTML output
    def __init__(self, path=""):
        self._path = path
        self._parser = self.construct_bnf()

        # the interal log
        self._log = ""
        # the attributes to be accessed from outside
        self.errors = []
        self.warnings = []
        self.state = None  # undefined until we find an error or a warning line

    def construct_bnf(self):
        """ The EBNF for a gcc log file."""

        # error/warning/note
        ewn_start = Word(alphanums + '_-./+') + Literal(':') + Optional(Word(nums) + Literal(':') + Word(nums) + Literal(':'))

        types = (Literal('fatal error').setParseAction(replaceWith("error")) | Literal('error') | Literal('warning') | Literal('In instantiation') | Literal('required from') | Literal('note') |
                 (Literal('In') + Optional(Literal('static')) + Optional(Literal('member')) + Literal('function')) | Literal('At global scope'))
        ewn = (ewn_start.suppress() + types + restOfLine).setParseAction(self.foundMessage)

        # type of lines that define the start for a new message
        newline = (Literal("In file included from") + Word(alphanums + '_-./+') + Literal(':') + restOfLine).setParseAction(self.newMessage)

        # other types of lines
        newline_f = (Literal('from') + Word(alphanums + '_-./+') + Literal(':') + restOfLine).setParseAction(self.addLog)
        comment = (Word('# ') + restOfLine).setParseAction(self.addLog)
        pointer = Word(" ^").setParseAction(self.addLog)
        # basically an line of arbitrary characters
        code = OneOrMore(Word(printables)).setParseAction(self.addLog)

        # suppress summary
        make = (Literal("make") + restOfLine).suppress()

        # a line | marks the first match first if several match
        line = (ewn | newline | newline_f | comment | make | pointer | code) + LineEnd()
        return line

    def addLog(self, origString, loc, tokens):
        # TODO break the message if too long
        self._log += origString + '\n'

    def newMessage(self, origString, loc, tokens):
        if self.state == 'warning':
            self.warnings.append(self._log)
            self._log = ""
            self.state = 'was_warning'  # undefined until we find an error or a warning line
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif self.state == 'error':
            self.errors.append(self._log)
            self._log = ""
            self.state = 'was_error'  # undefined until we find an error or a warning line
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        else:
            self.addLog(origString=origString, loc=loc, tokens=tokens)

    def foundMessage(self, origString, loc, tokens):
        if self.state == 'warning' and tokens[0].strip() == 'error':
            self.warnings.append(self._log)
            self._log = ""
            self.state = 'error'  # get also possible following lines until newMessage
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif self.state == 'error' and tokens[0].strip() == 'error':
            self.errors.append(self._log)
            self._log = ""
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif self.state == 'error' and tokens[0].strip() == 'warning':
            self.errors.append(self._log)
            self._log = ""
            self.state = 'warning'  # get also possible following lines until newMessage
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif self.state == 'warning' and tokens[0].strip() == 'warning':
            self.warnings.append(self._log)
            self._log = ""
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif (not self.state or self.state.startswith('was_')) and tokens[0].strip() == 'warning':
            self.state = 'warning'
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif (not self.state or self.state.startswith('was_')) and tokens[0].strip() == 'error':
            self.state = 'error'
            self.addLog(origString=origString, loc=loc, tokens=tokens)
        elif tokens[0].strip() == 'In instantiation':
            self.newMessage(origString=origString, loc=loc, tokens=tokens)
        elif tokens[0].strip() == 'note' and self.state == 'was_error':
            self._log = self.errors[-1] + '\n' + self._log
            self.errors.pop()
            self.addLog(origString=origString, loc=loc, tokens=tokens)
            self.state = 'error'
        elif tokens[0].strip() == 'note' and self.state == 'was_warning':
            self._log = self.warnings[-1] + '\n' + self._log
            self.warnings.pop()
            self.addLog(origString=origString, loc=loc, tokens=tokens)
            self.state = 'warning'
        else:
            self.addLog(origString=origString, loc=loc, tokens=tokens)

    def compile(self, line):
        self._parser.parseString(line)


class GCCLogHTMLMarkup(object):
    """A parser for make output with gcc supplying HTML formatting"""
    # TODO provide message highlighting and direct HTML output
    def __init__(self, path=""):
        self._path = path
        self._parser = self.construct_bnf()
        # the interal log
        self._log = ""

    def construct_bnf(self):
        """ The EBNF for a gcc log message."""

        # error/warning/note
        ewn_start = Word(alphanums + '_-./+') + Literal(':') + Optional(Word(nums) + Literal(':') + Word(nums) + Literal(':'))

        types = (Literal('fatal error') | Literal('error') | Literal('warning') | Literal('In instantiation') | Literal('required from') | Literal('note') |
                 (Literal('In') + Optional(Literal('static')) + Optional(Literal('member')) + Literal('function')) | Literal('At global scope'))

        ewn = Group(ewn_start) + Group(types) + restOfLine

        other = OneOrMore(Word(printables))

        ewn.addParseAction(self.addEWN)
        other.addParseAction(self.addOther)

        # a line | marks the first match first if several match
        line = ewn | other

        return line

    def addOther(self, origString, loc, tokens):
        self._log += self.formatHTML(origString) + '\n'

    def addEWN(self, origString, loc, tokens):
        self._log += self.formatHTML("".join(tokens[0]), font_weight='bold') + " " + self.formatType(" ".join(tokens[1]), tokens[2]) + '\n'

    def formatType(self, s, rest):
        if s == 'error':
            return self.formatHTML(s + rest, color='red', font_weight='bold')
        if s == 'fatal error':
            return self.formatHTML(s + rest, color='red', font_weight='bold')
        if s == 'warning':
            return self.formatHTML(s + rest, color='green', font_weight='bold')
        else:
            return self.formatHTML(s + rest, color='grey', font_weight='bold')

    def formatHTML(self, s, color='black', background_color='transparent', font_weight='normal'):
        return "<span style='color:{};background-color:{};font-weight:{}'>{}</span>".format(color, background_color, font_weight, s)

    def compile(self, line):
        self._parser.parseString(line)


def formatHTML(s):
    """A function formatting a single gcc log message with HTML"""

    if not s:
        return ''

    parser = GCCLogHTMLMarkup()
    for line in s.splitlines():
        try:
            parser.compile(line)
        except:
            continue
    return parser._log
