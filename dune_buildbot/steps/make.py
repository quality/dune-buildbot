from buildbot.plugins import steps
from buildbot.steps.slave import CompositeStepMixin
from buildbot.status.results import FAILURE, SUCCESS, WARNINGS
from ..process.logparser import GCCLogParser, formatHTML
from ..process.logobserver import LineConsumerLogObserver


class Make(steps.ShellCommand, CompositeStepMixin):
    """A build step for detailed make output"""
    errors_counter = 0
    warnings_counter = 0

    def __init__(self, processors=1, **kwargs):
        self.processors = processors

        # call the parent constructor
        steps.ShellCommand.__init__(self, **kwargs)

        # provide a default command
        if not self.command:
            self.setCommand(['make', '-j' + processors, '-k', '--always-make'])

        # initilialize the output parser
        self.parser = GCCLogParser()

        # watch the standard output
        observer = LineConsumerLogObserver(self.stderrLogConsumer)
        self.addLogObserver('stdio', observer)

    # @defer.inlineCallbacks
    # def run(self):  ## new style buildstep
    #     steps.ShellCommand.run()

    def stderrLogConsumer(self):
        while True:
            stream, line = yield
            # parse the line if it's stdout
            if stream == 'e':
                try:
                    self.parser.compile(line)
                except:
                    continue

                # update it here for the possibility of live output (TODO)
                self.errors_counter = len(self.parser.errors)
                self.warnings_counter = len(self.parser.warnings)
                # update the step's status
                # self.updateSummary() # only available in new-style buildsteps

    def createSummary(self, log):
        # if there were any errors make the error log available
        if self.errors_counter:
            complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
            for log in self.parser.errors:
                complete_log += "<div style='background-color:lightgrey'><pre>" + formatHTML(log) + "</pre></div>\n"
            complete_log += "</body>\n</html>\n"

            self.addHTMLLog("errors: {}".format(len(self.parser.errors)), complete_log)

        # if there were any errors make the error log available
        if self.warnings_counter:
            complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
            for log in self.parser.warnings:
                complete_log += "<div style='background-color:lightgrey'><pre>" + formatHTML(log) + "</pre></div>\n"
            complete_log += "</body>\n</html>\n"

            self.addHTMLLog("warnings: {}".format(len(self.parser.warnings)), complete_log)

    def evaluateCommand(self, cmd):
        if cmd.didFail() or self.errors_counter:
            return FAILURE
        if self.warnings_counter:
            return WARNINGS
        return SUCCESS
