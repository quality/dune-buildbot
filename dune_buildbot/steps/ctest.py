from __future__ import division
from buildbot.plugins import steps
from buildbot.steps.slave import CompositeStepMixin
from buildbot.status.results import FAILURE, SUCCESS, WARNINGS
from ..process.logparser import CTestLogParser
from ..process.logobserver import LineConsumerLogObserver


class Test(steps.ShellCommand, CompositeStepMixin):
    """A build step for detailed ctest result output"""
    failed_test_counter = 0
    skipped_test_counter = 0

    def __init__(self, processors=1, **kwargs):
        self.processors = processors

        # call the parent constructor
        steps.ShellCommand.__init__(self, **kwargs)

        # provide a default command
        if not self.command:
            self.setCommand(['ctest', '-j' + processors, '-VV'])

        # initilialize the output parser
        self.parser = CTestLogParser()
        self.failed_test_counter = 0
        self.skipped_test_counter = 0

        # watch the standard output
        observer = LineConsumerLogObserver(self.failedTestLogConsumer)
        self.addLogObserver('stdio', observer)

    # @defer.inlineCallbacks
    # def run(self):  ## new style buildstep
    #     steps.ShellCommand.run()

    def failedTestLogConsumer(self):
        while True:
            stream, line = yield
            # parse the line if it's stdout
            if stream == 'o':
                try:
                    self.parser.compile(line)
                except:
                    continue

                # update it here for the possibility of live output (TODO)
                self.failed_test_counter = len(self.parser.failed_tests)
                self.skipped_test_counter = len(self.parser.skipped_tests)
                # update the step's status
                # self.updateSummary() # only available in new-style buildsteps

    def createSummary(self, log):
        # always add the summary as a separate log
        self.addCompleteLog("test summary", self.parser.summary)

        # if there were any failed tests make the log with errors available
        if self.failed_test_counter:
            id_generator = 0
            complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
            for test, log in list(self.parser.log.items()):
                if test in self.parser.failed_tests:
                    complete_log += "<h3 style='color:grey' id='id_{}'>Test {} {}! </h3>\n".format(id_generator, self.parser.test_names[test], self.parser.failed_tests[test])
                    complete_log += "<a href=#id_{}> (next test) </a>\n".format(id_generator + 1)
                    complete_log += "<div style='background-color:lightgrey'><pre>" + log + "</pre></div>\n"
                    id_generator = id_generator + 1
            # remove the last and thus invalid link
            complete_log.strip("<a href=#{}> (next test) </a>\n".format(id_generator))
            complete_log += "</body>\n</html>\n"

            # publish the log file
            percentage_failed = self.failed_test_counter / len(self.parser.log.items())
            self.addHTMLLog("failed tests: {} ({:.0%})".format(len(self.parser.failed_tests), percentage_failed), complete_log)

        # if there were any skipped tests make the log with (hopefully) a reason available
        if self.skipped_test_counter:
            id_generator = 0
            complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
            for test, log in list(self.parser.log.items()):
                if test in self.parser.skipped_tests:
                    complete_log += "<h3 style='color:grey' id='id_{}'>Test {} {}! </h3>\n".format(id_generator, self.parser.test_names[test], self.parser.skipped_tests[test])
                    complete_log += "<a href=#id_{}> (next test) </a>\n".format(id_generator + 1)
                    complete_log += "<div style='background-color:lightgrey'><pre>" + log + "</pre></div>\n"
                    id_generator = id_generator + 1
            # remove the last and thus invalid link
            complete_log.strip("<a href=#{}> (next test) </a>\n".format(id_generator))
            complete_log += "</body>\n</html>\n"

            # publish the log file
            percentage_skipped = self.skipped_test_counter / len(self.parser.log.items())
            self.addHTMLLog("skipped tests: {} ({:.0%})".format(len(self.parser.skipped_tests), percentage_skipped), complete_log)

        # set statistics and properties
        failed_tests_stat = self.getStatistic('failed-tests', 0)
        self.setStatistic('failed-tests', failed_tests_stat + len(self.parser.failed_tests))
        skipped_tests_stat = self.getStatistic('skipped-tests', 0)
        self.setStatistic('skipped-tests', skipped_tests_stat + len(self.parser.skipped_tests))

        old_count = self.getProperty("failed-tests-count", 0)
        self.setProperty("failed-tests-count", old_count + len(self.parser.failed_tests), "Test")
        old_count = self.getProperty("skipped-tests-count", 0)
        self.setProperty("skipped-tests-count", old_count + len(self.parser.skipped_tests), "Test")

    def evaluateCommand(self, cmd):
        if self.failed_test_counter:
            return FAILURE
        return SUCCESS
