from dune_buildbot.process.logparser import GCCLogParser, formatHTML
from pyparsing import ParseException


def test_warnings(dir):

    parser = GCCLogParser()
    file = open(dir + 'log.txt')
    for line in file:
        try:
            parser.compile(line)
        except ParseException as err:
            print("ParseException:")
            print(err.line)
            print(" " * (err.column - 1) + "^")
            print(err)
    print("errors: ", len(parser.errors))
    print("warnings: ", len(parser.warnings))

    assert(len(parser.warnings) == 161)

    complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
    for log in parser.warnings:
        complete_log += "<div style='background-color:lightgrey'><pre>" + formatHTML(log) + "</pre></div>\n"
    complete_log += "</body>\n</html>\n"


def test_errors(dir):

    parser = GCCLogParser()
    file = open(dir + 'log2.txt')
    for line in file:
        try:
            parser.compile(line)
        except ParseException as err:
            print("ParseException:")
            print(err.line)
            print(" " * (err.column - 1) + "^")
            print(err)
    print("errors: ", len(parser.errors))
    print("warnings: ", len(parser.warnings))

    assert(len(parser.warnings) == 16)
    assert(len(parser.errors) == 2)

    complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
    for log in parser.warnings:
        complete_log += "<div style='background-color:lightgrey'><pre>" + formatHTML(log) + "</pre></div>\n"
    complete_log += "</body>\n</html>\n"

    complete_log = "<!DOCTYPE html>\n<html>\n<head>\n<meta charset='UTF-8'>\n</head>\n<body>\n"
    for log in parser.errors:
        complete_log += "<div style='background-color:lightgrey'><pre>" + formatHTML(log) + "</pre></div>\n"
    complete_log += "</body>\n</html>\n"

    # text_file = open("errors.html", "w")
    # text_file.write(complete_log)
    # text_file.close()
